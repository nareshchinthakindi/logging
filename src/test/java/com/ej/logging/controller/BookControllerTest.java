package com.ej.logging.controller;

import com.ej.logging.model.Book;
import com.ej.logging.repo.BookRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.test.web.servlet.MockMvc;

@ExtendWith(MockitoExtension.class)
public class BookControllerTest {

    private MockMvc mockMvc;

    private ObjectMapper mapper = new ObjectMapper();
    private ObjectWriter writer = mapper.writer();

    @Mock
    private BookRepository repository;

    @InjectMocks
    private BookController controller;

    Book RECORD_1 = new Book(1L,"Java", 1, "SUM1");
}
