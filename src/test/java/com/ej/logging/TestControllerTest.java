package com.ej.logging;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;

import java.net.URISyntaxException;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class TestControllerTest {

    @Autowired
    TestRestTemplate testRestTemplate;

    @Test
    public void testUrlB() throws URISyntaxException {
        HttpHeaders headers = new HttpHeaders();
        headers.set("X-COM-PERSIST", "true");
        HttpEntity<String> request = new HttpEntity<>(headers);
        ResponseEntity<String> result = testRestTemplate.getForEntity("/test", String.class, request);
        //Verify request succeed
        Assertions.assertEquals(200, result.getStatusCodeValue());
    }

    @Test
    public void testUrl() throws URISyntaxException {
        HttpHeaders headers = new HttpHeaders();
        headers.set("X-COM-PERSIST", "true");
        HttpEntity<String> request = new HttpEntity<>(headers);
        ResponseEntity<String> result = testRestTemplate.getForEntity("/test/junit5", String.class, request);
        //Verify request succeed
        Assertions.assertEquals(200, result.getStatusCodeValue());
    }

}
