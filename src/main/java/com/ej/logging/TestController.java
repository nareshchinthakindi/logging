package com.ej.logging;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/test")
public class TestController {


    @LogExecution
    @GetMapping
    public String helloWorld() {
        return "Welcome!";
    }

    @GetMapping("/junit5")
    public String junit5() {
        return "Welcome!";
    }
}
