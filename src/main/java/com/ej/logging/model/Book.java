package com.ej.logging.model;


import lombok.*;

import javax.persistence.*;

@Data
@Entity
@RequiredArgsConstructor
@AllArgsConstructor
@Table(name ="book_record")
@Builder
public class Book {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @NonNull
    private String name;

    private Integer rating;

   @NonNull
    private String summary;

}
