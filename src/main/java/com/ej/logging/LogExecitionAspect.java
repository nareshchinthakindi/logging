package com.ej.logging;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class LogExecitionAspect {

    @Around("@annotation(com.ej.logging.LogExecution)")
    public Object methodExecute(ProceedingJoinPoint proceedingJoinPoint) throws Throwable {
        System.out.println("Before calling");
        return proceedingJoinPoint.proceed();
    }
}
