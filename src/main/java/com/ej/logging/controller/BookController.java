package com.ej.logging.controller;

import com.ej.logging.model.Book;
import com.ej.logging.repo.BookRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping(value = "/book")
public class BookController {

    @Autowired
    private BookRepository repository;

    @GetMapping
    public List<Book> getAllBooks() {
        return repository.findAll();
    }

    @GetMapping(value = "{bookId}")
    public Book bookById(@PathVariable(value = "bookId") Long bookId) {
        return repository.findById(bookId).orElse(Book.builder().build());
    }

    @PostMapping
    public Book addNewBook(@Validated @RequestBody Book book) {
        return repository.save(book);
    }

    @PutMapping
    public Book update(@Validated @RequestBody Book book) throws Exception {
      if (book == null || book.getId() == null) {
          throw new IllegalStateException();
      }

      Optional<Book> dbBook = repository.findById(book.getId());

      if (dbBook.isEmpty()) {
          throw new Exception("Not Found "+book.getId());
      }

      Book existingBook = dbBook.get();

      existingBook.setName(book.getName());
      existingBook.setRating(book.getRating());
      existingBook.setSummary(book.getSummary());

      return repository.save(existingBook);

    }
}
