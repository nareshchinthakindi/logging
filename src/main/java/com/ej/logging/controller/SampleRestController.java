package com.ej.logging.controller;

import com.ej.logging.model.Book;
import com.ej.logging.repo.BookRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/sample")
public class SampleRestController {

    @Autowired
    BookRepository repository;

    @GetMapping
    public List<Book> getAllBooks() {
        return repository.findAll();
    }


    @PostMapping
    public Book createNewBook(@RequestBody @Validated Book book) {
        return repository.save(book);
    }

    @PostMapping
    public Book updateBook(@RequestBody @Validated Book book) {
        return repository.save(book);
    }
}
